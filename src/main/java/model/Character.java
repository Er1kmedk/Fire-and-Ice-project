package model;

import org.json.JSONArray;

public class Character {
    private String url;
    private String name;
    private String gender;
    private String culture;
    private String born;
    private String died;
    private JSONArray allegiances;

    public Character(String url, String name, String gender, String culture, String born, String died, JSONArray allegiances) {
        this.url = url;
        this.name = name;
        this.gender = gender;
        this.culture = culture;
        this.born = born;
        this.died = died;
        this.allegiances = allegiances;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getCulture() {
        return culture;
    }

    public String getBorn() {
        return born;
    }

    public String getDied() {
        return died;
    }

    public JSONArray getAllegiances() {
        return allegiances;
    }
}
