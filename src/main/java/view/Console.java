package view;

import controller.FireAndIceManager;

import java.util.ArrayList;
import java.util.Scanner;

public class Console {
    public static void main(String[] args) {
        FireAndIceManager faiManager = new FireAndIceManager();
        Scanner inputScanner = new Scanner(System.in);

        System.out.println("Enter character number: \n");
        //int characterNumber = inputScanner.nextInt();
        String characterNumber = "520";

        // This code calculates how mane "-" is needed to surround the prints.
        int total = 40;
        int characterString = ("Character: " + characterNumber).length();
        if ((total - characterString) % 2 == 1) {
            System.out.println((total - characterString) % 2 == 1);
            System.out.println("--------------" + "Character: " + characterNumber + "-------------");
        }
        else {
            System.out.println("-------------" + "Character: " + characterNumber + "-------------");
        }

        System.out.println("Name: " + faiManager.getCharacter(characterNumber).getName());
        System.out.println("Gender: " + faiManager.getCharacter(characterNumber).getGender());
        System.out.println("Culture: " + faiManager.getCharacter(characterNumber).getCulture());
        System.out.println("Born: " + faiManager.getCharacter(characterNumber).getBorn());
        System.out.println("Died: " + faiManager.getCharacter(characterNumber).getDied());
        System.out.println("----------------------------------------\n");


        System.out.println("Display character " + faiManager.getCharacter(characterNumber).getName() + "'s house members? (Yes/No)");
        String userInputTwo = "No";
//        userInputTwo = inputScanner.nextLine();
        if (userInputTwo == "Yes") {
            ArrayList<String> memberNameList = faiManager.getHouseMembers(characterNumber);
            for(String name : memberNameList) {
                System.out.println(name);
            }
        }

        faiManager.getBantamBookPOVCharacters();

    }
}
