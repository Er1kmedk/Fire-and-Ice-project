package controller;

import com.google.gson.JsonArray;
import model.Character;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

public class FireAndIceManager {


    public FireAndIceManager() {

    }

    public Character getCharacter(String characterNumber) {
        Character requestedCharacter = null;
        try {
            URL url = new URL ("https://anapioficeandfire.com/api/characters/" + characterNumber);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");

            if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStreamReader inputStreamReader = new InputStreamReader((urlConnection.getInputStream()));
                BufferedReader buffReader = new BufferedReader((inputStreamReader));

                String inputLine;
                StringBuffer content = new StringBuffer();

                while ((inputLine = buffReader.readLine()) != null) {
                    content.append(inputLine);
                }
                buffReader.close();

                JSONObject json = new JSONObject(content.toString());
                requestedCharacter = createCharacter(json);

            }
            else {
                System.out.println("Error");
                System.out.println("Server responded with: " + urlConnection.getResponseCode());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return requestedCharacter;
    }


    // Created a character with the basic information from the json object
    private Character createCharacter(JSONObject jsonObject) {
        return new Character(jsonObject.get("url").toString(),
                jsonObject.get("name").toString(),
                jsonObject.get("gender").toString(),
                jsonObject.get("culture").toString(),
                jsonObject.get("born").toString(),
                jsonObject.get("died").toString(),
                jsonObject.getJSONArray("allegiances"));
    }


    public ArrayList<String> getHouseMembers(String characterNumber) {
        Character thisCharacter = getCharacter(characterNumber);
        JSONArray allegiancesArray = thisCharacter.getAllegiances();

        // Iterate through all allegiances and take the URl for each of them to further access the underlying members.
        for (Object allegiances : allegiancesArray) {
            try {
                URL characterHouseURL = new URL(allegiances.toString());
                HttpURLConnection urlConnection = (HttpURLConnection) characterHouseURL.openConnection();
                urlConnection.setRequestMethod("GET");

                if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    InputStreamReader inputStreamReader = new InputStreamReader((urlConnection.getInputStream()));
                    BufferedReader buffReader = new BufferedReader((inputStreamReader));

                    String inputLine;
                    StringBuffer content = new StringBuffer();

                    while ((inputLine = buffReader.readLine()) != null) {
                        content.append(inputLine);
                    }
                    buffReader.close();

                    JSONObject houseObject = new JSONObject(content.toString());
                    JSONArray houseArray = houseObject.getJSONArray("swornMembers");
                    ArrayList<String> houseMemberNames = new ArrayList<String>();

                    // Since all the character URLs look the same for the first 44 character we
                    // only need to extract the unique number from the end of the String.
                    for (Object memberURL : houseArray) {
                        Character character = getCharacter((memberURL.toString().substring(45)));
                        houseMemberNames.add(character.getName());
                    }
                    return houseMemberNames;
                } else {
                    System.out.println("Error");
                    System.out.println("Server responded with: " + urlConnection.getResponseCode());
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
            return null;
    }


    public void getBantamBookPOVCharacters() {
        try {
            URL url = new URL ("https://anapioficeandfire.com/api/books/");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");

            if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStreamReader inputStreamReader = new InputStreamReader((urlConnection.getInputStream()));
                BufferedReader buffReader = new BufferedReader((inputStreamReader));

                String inputLine;
                StringBuffer content = new StringBuffer();

                while ((inputLine = buffReader.readLine()) != null) {
                    content.append(inputLine);
                    //System.out.println(content);
                }
                buffReader.close();

                JSONArray library = new JSONArray(content.toString());
                ArrayList<String> bookNameArray = new ArrayList<String>();
                ArrayList<String> characterArray = new ArrayList<String>();


                for (int i = 0; i < library.length(); i++) {
                    JSONObject book = library.getJSONObject(i);

                    if (book.getString("publisher").equals("Bantam Books")) {
                        System.out.println("Book: " + book.getString("name") + "\nPOVs: ");
                        bookNameArray.add(book.getString("name"));
                        JSONArray povs = book.getJSONArray("povCharacters");

                        for (Object characterURL : povs) {
                            Character character = getCharacter(characterURL.toString().substring(45));
                            characterArray.add(character.getName());
                            System.out.println(character.getName());
                        }
                        System.out.println("\n");
                    }
                }

                for (String book : bookNameArray) {
//
//                    System.out.println(characterArray.size());
                    for (int i = 0; i < characterArray.size(); i++) {
                        //System.out.println(characterArray.get(i));
                    }
                }
                System.out.println("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
